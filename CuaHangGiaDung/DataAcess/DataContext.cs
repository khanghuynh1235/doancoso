﻿using CuaHangGiaDung.Models;
using Microsoft.EntityFrameworkCore;

namespace CuaHangGiaDung.DataAcess
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<ChiTietDonHang> ChiTietDonHangs { get; set; }
        public DbSet<DonHang> DonHangs { get; set; }
        public DbSet<LoaiHang> LoaiHangs { get; set; }
        public DbSet<NhaCungCap> NhaCungCaps { get; set; }
        public DbSet<PhanQuyen> PhanQuyens { get; set; }
        public DbSet<SanPham> SanPhams { get; set; }
        public DbSet<TaiKhoan> TaiKhoans { get; set; }
        public DbSet<TinTuc> TinTucs { get; set; }
    }
}
