﻿using CuaHangGiaDung.DataAcess;
using CuaHangGiaDung.Models;
using Microsoft.EntityFrameworkCore;

namespace CuaHangGiaDung.Repositories
{
    public class EFProductRepository : IProductRepository
    {
        private readonly DataContext _context;

        public EFProductRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SanPham>> GetAllAsync()
        {
            // return await _context.Products.ToListAsync();
            return await _context.SanPhams
        .Include(p => p.LoaiHang) // Include thông tin về category
        .ToListAsync();

        }

        public async Task<SanPham> GetByIdAsync(int id)
        {
            // return await _context.Products.FindAsync(id);
            // lấy thông tin kèm theo category
            return await _context.SanPhams.Include(p => p.LoaiHang).FirstOrDefaultAsync(p => p.MaSP == id);
        }

        public async Task AddAsync(SanPham product)
        {
            _context.SanPhams.Add(product);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(SanPham product)
        {
            _context.SanPhams.Update(product);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var product = await _context.SanPhams.FindAsync(id);
            _context.SanPhams.Remove(product);
            await _context.SaveChangesAsync();
        }

    }

}
