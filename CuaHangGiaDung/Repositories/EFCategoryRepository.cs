﻿using CuaHangGiaDung.DataAcess;
using Microsoft.EntityFrameworkCore;
using CuaHangGiaDung.Models;


namespace CuaHangGiaDung.Repositories
{
    public class EFCategoryRepository : ICategoryRepository
    {
        private DataContext _context;

        public EFCategoryRepository(DataContext context)
        {
            _context = context;
        }

        // Tương tự như EFProductRepository, nhưng cho Category

        public async Task<IEnumerable<LoaiHang>> GetAllAsync()
        {
            // return await _context.Products.ToListAsync();
            return await _context.LoaiHangs
        .Include(p => p.SanPhams) // Include thông tin về category
        .ToListAsync();

        }
        public async Task<LoaiHang> GetByIdAsync(int id)
        {
            // return await _context.Products.FindAsync(id);
            // lấy thông tin kèm theo category
            return await _context.LoaiHangs.Include(p => p.SanPhams).FirstOrDefaultAsync(p => p.MaLoai == id);
        }

        public async Task AddAsync(LoaiHang category)
        {
            _context.LoaiHangs.Add(category);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(LoaiHang category)
        {
            _context.LoaiHangs.Update(category);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var category = await _context.LoaiHangs.FindAsync(id);
            _context.LoaiHangs.Remove(category);
            await _context.SaveChangesAsync();
        }
    }

}
