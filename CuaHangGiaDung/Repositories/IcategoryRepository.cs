﻿using CuaHangGiaDung.Models;

namespace CuaHangGiaDung.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<LoaiHang>> GetAllAsync();
        Task<LoaiHang> GetByIdAsync(int id);
        Task AddAsync(LoaiHang category);
        Task UpdateAsync(LoaiHang category);
        Task DeleteAsync(int id);
    }

}
