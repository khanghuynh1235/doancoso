﻿using CuaHangGiaDung.Models;

namespace CuaHangGiaDung.Repositories
{
    public interface IProductRepository
    {
        Task<IEnumerable<SanPham>> GetAllAsync();
        Task<SanPham> GetByIdAsync(int id);
        Task AddAsync(SanPham product);
        Task UpdateAsync(SanPham product);
        Task DeleteAsync(int id);
    }

}
