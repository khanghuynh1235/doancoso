﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class TinTuc
    {
        [Key]
        [Display(Name = "Mã tin tức")]
        public int MaTT { get; set; }

        [StringLength(100)]
        [Display(Name = "Tiêu đề")]
        public string TieuDe { get; set; }

        [Display(Name = "Nội dung")]
        [Column(TypeName = "ntext")]
        public string NoiDung { get; set; }
    }
}
