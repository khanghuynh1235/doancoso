﻿using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class PhanQuyen
    {
        public PhanQuyen()
        {
            TaiKhoans = new HashSet<TaiKhoan>();
        }

        [Display(Name = "ID quyền")]
        [Key]
        public int IDQuyen { get; set; }

        [Display(Name = "Tên quyền")]
        [StringLength(20)]
        public string TenQuyen { get; set; }

        public virtual ICollection<TaiKhoan> TaiKhoans { get; set; }
    }
}
