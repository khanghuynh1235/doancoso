﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class SanPham
    {
        public SanPham()
        {
            ChiTietDonHangs = new HashSet<ChiTietDonHang>();
        }

        [Display(Name = "Mã sản phẩm")]
        [Key]
        public int MaSP { get; set; }

        [Display(Name = "Tên sản phẩm")]
        [StringLength(100)]
        public string TenSP { get; set; }

        [Display(Name = "Giá bán")]
        public decimal? GiaBan { get; set; }

        [Display(Name = "Số lượng")]
        public int? Soluong { get; set; }

        [Display(Name = "Mô tả")]
        [Column(TypeName = "ntext")]
        public string MoTa { get; set; }

        [Display(Name = "Mã loại")]
        public int? MaLoai { get; set; }

        [Display(Name = "Nhà cung cấp")]
        public int? MaNCC { get; set; }

        [Display(Name = "Ảnh bìa")]
        [StringLength(100)]
        public string AnhSP { get; set; }

        public virtual ICollection<ChiTietDonHang> ChiTietDonHangs { get; set; }

        public virtual LoaiHang LoaiHang { get; set; }

        public virtual NhaCungCap NhaCungCap { get; set; }
    }
}
