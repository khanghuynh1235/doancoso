﻿using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class LoaiHang
    {
        public LoaiHang()
        {
            SanPhams = new HashSet<SanPham>();
        }

        [Display(Name = "Mã loại hàng")]
        [Key]
        public int MaLoai { get; set; }

        [Display(Name = "Tên loại hàng")]
        [StringLength(100)]
        public string TenLoai { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }
    }
}
