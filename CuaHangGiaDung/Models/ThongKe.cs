﻿namespace CuaHangGiaDung.Models
{
    public class ThongKe
    {
        public string Tennguoidung { get; set; }
        public string Dienthoai { get; set; }
        public decimal? Tongtien { get; set; }
        public int? Soluong { get; set; }
    }
}
