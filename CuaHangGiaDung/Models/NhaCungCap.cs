﻿using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class NhaCungCap
    {
        public NhaCungCap()
        {
            SanPhams = new HashSet<SanPham>();
        }

        [Display(Name = "Mã nhà cung cấp")]
        [Key]
        public int MaNCC { get; set; }

        [Display(Name = "Tên nhà cung cấp")]
        [StringLength(100)]
        public string TenNCC { get; set; }

        public virtual ICollection<SanPham> SanPhams { get; set; }
    }
}
