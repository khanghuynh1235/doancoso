﻿using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class DonHang
    {
        [Display(Name = "Mã đơn hàng")]
        [Key]
        public int MaDon { get; set; }

        [Display(Name = "Ngày đặt")]
        public DateTime? NgayDat { get; set; }

        [Display(Name = "Tình trạng đơn hàng")]
        public int? TinhTrang { get; set; }

        [Display(Name = "Hình thức thanh toán")]
        public int? ThanhToan { get; set; }

        [Display(Name = "Địa chỉ nhận hàng")]
        [StringLength(100)]
        public string DiaChiNhanHang { get; set; }

        [Display(Name = "Người đặt")]
        public int? MaNguoiDung { get; set; }

        [Display(Name = "Tổng tiền")]
        public decimal? TongTien { get; set; }

        public virtual ICollection<ChiTietDonHang> ChiTietDonHangs { get; set; }

        public virtual TaiKhoan TaiKhoan { get; set; }
    }
}
