﻿using System.ComponentModel.DataAnnotations;

namespace CuaHangGiaDung.Models
{
    public class DangNhap
    {
        [Key]
        [Display(Name = "Email")]
        public string userMail { get; set; }
        [Display(Name = "Mật khẩu")]
        public string password { get; set; }
    }
}
