﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CuaHangGiaDung.Migrations
{
    /// <inheritdoc />
    public partial class NewMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoaiHangs",
                columns: table => new
                {
                    MaLoai = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoai = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiHangs", x => x.MaLoai);
                });

            migrationBuilder.CreateTable(
                name: "NhaCungCaps",
                columns: table => new
                {
                    MaNCC = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenNCC = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhaCungCaps", x => x.MaNCC);
                });

            migrationBuilder.CreateTable(
                name: "PhanQuyens",
                columns: table => new
                {
                    IDQuyen = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenQuyen = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhanQuyens", x => x.IDQuyen);
                });

            migrationBuilder.CreateTable(
                name: "TinTucs",
                columns: table => new
                {
                    MaTT = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TieuDe = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    NoiDung = table.Column<string>(type: "ntext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TinTucs", x => x.MaTT);
                });

            migrationBuilder.CreateTable(
                name: "SanPhams",
                columns: table => new
                {
                    MaSP = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenSP = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GiaBan = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Soluong = table.Column<int>(type: "int", nullable: true),
                    MoTa = table.Column<string>(type: "ntext", nullable: false),
                    MaLoai = table.Column<int>(type: "int", nullable: true),
                    MaNCC = table.Column<int>(type: "int", nullable: true),
                    AnhSP = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LoaiHangMaLoai = table.Column<int>(type: "int", nullable: false),
                    NhaCungCapMaNCC = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SanPhams", x => x.MaSP);
                    table.ForeignKey(
                        name: "FK_SanPhams_LoaiHangs_LoaiHangMaLoai",
                        column: x => x.LoaiHangMaLoai,
                        principalTable: "LoaiHangs",
                        principalColumn: "MaLoai",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SanPhams_NhaCungCaps_NhaCungCapMaNCC",
                        column: x => x.NhaCungCapMaNCC,
                        principalTable: "NhaCungCaps",
                        principalColumn: "MaNCC",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaiKhoans",
                columns: table => new
                {
                    MaNguoiDung = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HoTen = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Dienthoai = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Matkhau = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    IDQuyen = table.Column<int>(type: "int", nullable: true),
                    Diachi = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    PhanQuyenIDQuyen = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaiKhoans", x => x.MaNguoiDung);
                    table.ForeignKey(
                        name: "FK_TaiKhoans_PhanQuyens_PhanQuyenIDQuyen",
                        column: x => x.PhanQuyenIDQuyen,
                        principalTable: "PhanQuyens",
                        principalColumn: "IDQuyen",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DonHangs",
                columns: table => new
                {
                    MaDon = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NgayDat = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TinhTrang = table.Column<int>(type: "int", nullable: true),
                    ThanhToan = table.Column<int>(type: "int", nullable: true),
                    DiaChiNhanHang = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    MaNguoiDung = table.Column<int>(type: "int", nullable: true),
                    TongTien = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TaiKhoanMaNguoiDung = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonHangs", x => x.MaDon);
                    table.ForeignKey(
                        name: "FK_DonHangs_TaiKhoans_TaiKhoanMaNguoiDung",
                        column: x => x.TaiKhoanMaNguoiDung,
                        principalTable: "TaiKhoans",
                        principalColumn: "MaNguoiDung",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietDonHangs",
                columns: table => new
                {
                    CTMaDon = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaDon = table.Column<int>(type: "int", nullable: false),
                    MaSP = table.Column<int>(type: "int", nullable: false),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    DonGia = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ThanhTien = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    PhuongThucThanhToan = table.Column<int>(type: "int", nullable: true),
                    DonHangMaDon = table.Column<int>(type: "int", nullable: false),
                    SanPhamMaSP = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiTietDonHangs", x => x.CTMaDon);
                    table.ForeignKey(
                        name: "FK_ChiTietDonHangs_DonHangs_DonHangMaDon",
                        column: x => x.DonHangMaDon,
                        principalTable: "DonHangs",
                        principalColumn: "MaDon",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietDonHangs_SanPhams_SanPhamMaSP",
                        column: x => x.SanPhamMaSP,
                        principalTable: "SanPhams",
                        principalColumn: "MaSP",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDonHangs_DonHangMaDon",
                table: "ChiTietDonHangs",
                column: "DonHangMaDon");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietDonHangs_SanPhamMaSP",
                table: "ChiTietDonHangs",
                column: "SanPhamMaSP");

            migrationBuilder.CreateIndex(
                name: "IX_DonHangs_TaiKhoanMaNguoiDung",
                table: "DonHangs",
                column: "TaiKhoanMaNguoiDung");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_LoaiHangMaLoai",
                table: "SanPhams",
                column: "LoaiHangMaLoai");

            migrationBuilder.CreateIndex(
                name: "IX_SanPhams_NhaCungCapMaNCC",
                table: "SanPhams",
                column: "NhaCungCapMaNCC");

            migrationBuilder.CreateIndex(
                name: "IX_TaiKhoans_PhanQuyenIDQuyen",
                table: "TaiKhoans",
                column: "PhanQuyenIDQuyen");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChiTietDonHangs");

            migrationBuilder.DropTable(
                name: "TinTucs");

            migrationBuilder.DropTable(
                name: "DonHangs");

            migrationBuilder.DropTable(
                name: "SanPhams");

            migrationBuilder.DropTable(
                name: "TaiKhoans");

            migrationBuilder.DropTable(
                name: "LoaiHangs");

            migrationBuilder.DropTable(
                name: "NhaCungCaps");

            migrationBuilder.DropTable(
                name: "PhanQuyens");
        }
    }
}
